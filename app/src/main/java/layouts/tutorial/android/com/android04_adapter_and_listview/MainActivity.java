package layouts.tutorial.android.com.android04_adapter_and_listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import layouts.tutorial.android.com.android04_adapter_and_listview.Listeners.SpinnerListener;

public class MainActivity extends AppCompatActivity {
    private TextView selection;
    private ListView listView;
    private String[] items = {"lorem", "ipsum", "dolor", "sit", "amet", "lorem", "ipsum", "dolor", "sit", "amet", "lorem", "ipsum", "dolor", "sit", "amet"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get textView object from xml
        selection = (TextView) findViewById(R.id.selection);

        //get listView object from xml
        listView = (ListView) findViewById(R.id.list);

        //define and set ArrayAdapter
        //ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        //************************************************

        //listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        //ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_single_choice, items);
        //************************************************

        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, items);

        listView.setAdapter(arrayAdapter);

        //list view item click listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //set text to textView
                if(listView.getChoiceMode() == AbsListView.CHOICE_MODE_MULTIPLE){
                    selection.setText("Number of selected items: " + listView.getCheckedItemCount());
                }
                else{
                    selection.setText(items[position]);
                }

                //show message
                Toast.makeText(getApplicationContext(), "Position: " + position + "  Item: " + items[position], Toast.LENGTH_SHORT).show();
            }
        });


        //Spinner set up
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new SpinnerListener());

        ArrayAdapter<String> arrayAdapterSpinner = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        arrayAdapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapterSpinner);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
